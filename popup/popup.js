var gettingItem = browser.storage.local.get("enabled");
gettingItem.then(onGot, onError);

function onGot(item) {
  var button = document.getElementById("button01")
  if (item.enabled == null) {
    browser.storage.local.set({ enabled: {status: true}})
  } else {
    set_status(item.enabled.status);
  }
  button.addEventListener("click", onClick);
}

function onError(error) {
  console.log(`Error: ${error}`)
}

function onClick() {
  var button = document.getElementById("button01")
  if (button.className == "button enabled") {
    set_status(false);
    browser.storage.local.set({ enabled: {status: false}})
  } else if (button.className == "button disabled") {
    set_status(true);
    browser.storage.local.set({ enabled: {status: true}})
  }
}

function set_status(status_to_set) {
  var button = document.getElementById("button01")
  if (status_to_set == true) {
    button.className= "button enabled";
    button.innerHTML = "Enabled";
    browser.browserAction.setIcon({path: "../icons/GitlabProtect-on-48.png"})
  } else {
    button.className= "button disabled";
    button.innerHTML = "Disabled";
    browser.browserAction.setIcon({path: "../icons/GitlabProtect-off-48.png"});
  }
}

var button = document.getElementById("button01")
button.addEventListener("click", onClick);
