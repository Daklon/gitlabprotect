console.log("GItlabProtect Loaded")
var gettingItem = browser.storage.local.get("enabled");
gettingItem.then(onGot, onError);

function onGot(item) {
  if (item.enabled == null) {
    onError("undefined")
  } else if (item.enabled.status == true) {
    setTimeout(function(){
      console.log("GitlabProtect working")
      var merge_button = document.getElementsByClassName("btn qa-merge-button accept-merge-request btn-success")
      var author = document.getElementsByClassName("author-link js-user-link d-none d-sm-inline")[0].getAttribute("data-username")
      var user = document.getElementsByClassName("header-user-dropdown-toggle")[0].pathname.substring(1)
      if (author != user) {
        var new_merge = merge_button[0].cloneNode(true)
        merge_button[0].parentNode.replaceChild(new_merge, merge_button[0])
      }
    }, 3000)
  }
}

function onError(error) {
  console.log(`Error: ${error}`)
  browser.storage.local.set({
    enabled: {status: true}
  })
  location.reload();
}
